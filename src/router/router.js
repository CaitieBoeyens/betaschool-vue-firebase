import Vue from "vue";
import Router from "vue-router";

import Home from "../views/Home.vue";
import Welcome from "../views/Welcome.vue";
import Profile from "../views/Profile.vue";
import Login from "../views/Login.vue";
import ManageClasses from "../views/ManageClasses.vue";
import ManageMarks from "../views/ManageMarks.vue";
import ManageFees from "../views/ManageFees.vue";
import ManageAttendance from "../views/ManageAttendance.vue";
import ManageUsers from "../views/ManageUsers.vue";


import store from "../store";
Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: "/",
      name: "welcome",
      component: Welcome
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        auth: false
      }
    },
    {
      path: "/dashboard",
      name: "home",
      component: Home,
      meta: {
        auth: true
      }
    },
    {
      path: "/profile",
      name: "profile",
      component: Profile
    },
    {
      path: "/classes",
      component: ManageClasses,
      name: "classes"
    },
    {
      path: "/marks",
      name: "marks",
      component: ManageMarks
    },
    {
      path: "/attendance",
      name: "attendance",
      component: ManageAttendance
    },
    {
      path: "/fees",
      name: "fees",
      component: ManageFees
    },
    {
      path: "/users",
      name: "users",
      component: ManageUsers
    }
  ]
});



router.beforeEach((to, from, next) => {
  const is_logged_in = store.getters["Auth/user_is_authenticated"];
  /* const admin = store.state.Auth.userRole === "admin";
  const lecturer = store.state.Auth.userRole === "lecturer"; */
  if (
    is_logged_in ||
    to.name === "login" ||
    to.name === "register" ||
    to.name === "welcome"
  ) {
    next();
    /* if (
      admin &&
      (to.name === "classes" || to.name === "fees" || to.name === "users")
    ) {
      next();
      
    } else {
      next("/dashboard");
    }
    if (lecturer && (to.name === "marks" || to.name === "attendance")) {
      next();
    } else {
      next("/dashboard");
    } */
  } else {
    next("/login");
  }
});

export default router;
