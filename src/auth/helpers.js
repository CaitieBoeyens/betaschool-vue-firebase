import store from "../store";

export default class AuthHelper {
  static token_is_valid(token) {
    const token_regex = /(Bearer )([0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+)/g;
    return token_regex.test(token);
  }

  static auth_token() {
    return store.state.Auth.auth_token || "";
  }
  /* 
  static currently_at_login() {
    const current_route = router.currentRoute.path || "";
    return current_route === "/login";
  } */

  static getClaimsFromToken(token) {
    if (!token) throw new TypeError(`expected token_string, got ${token}`);
    const token_regex = /(?:Bearer )(?:[0-9a-zA-Z-_]+).([0-9a-zA-Z-_]+).(?:[0-9a-zA-Z-_]+)/g;
    const results = token_regex.exec(token);
    const claims_base64_string = results[1] || "e30="; //base64 {}
    const claims = JSON.parse(window.atob(claims_base64_string));
    return typeof claims === "object" ? claims : {};
  }

  /* static checkTokenInRefreshLifetime(token) {
    const { iat: issued_at } = AuthHelper.getClaimsFromToken(token);
    const now = Date.now() / 1000;
    //1000 converts to seconds, Date.now in ms -> s 
    return now < issued_at + AuthenticationConfig.token_refresh_lifetime;
  } */
}
