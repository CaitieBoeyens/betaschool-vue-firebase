/* eslint-disable no-console */
import axios from "@/ajax.js";

const state = {
  class: {
    id: null,
    term: null,
    week: null,
    students: [],
    average: null,
    name: null
  }
};

const mutations = {
  SET_CLASS(state, fullClass) {
    state.class.id = fullClass.id;
    state.class.name = fullClass.name;
  },
  SET_STUDENTS(state, students) {
    students.forEach(student => {
      state.class.students.push({
        ...student,
        attendance: false
      });
    });
  },
  SET_TERM(state, term) {
    state.class.term = term;
  },
  SET_WEEK(state, week) {
    state.class.week = week;
  },
  SET_ATTENDANCE(state, data) {
    state.class.students.forEach(student => {
      if (student.user_id === data.user_id) {
        student.attendance = !student.attendance;
      }
    });
  },
  RESET_CLASS(state) {
    state.class = {
      id: null,
      term: null,
      week: null,
      students: [],
      average: null,
      subject: null
    };
  }
};

const actions = {
  /* getStudents({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/${state.class.id}/students`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_STUDENTS", response.data);
      })
      .catch(err => console.log(err));
  }, */
  setClass({ commit }, fullClass) {
    commit("SET_CLASS", fullClass);
    commit("SET_STUDENTS", fullClass.students);
  },
  setTerm({ commit }, term) {
    commit("SET_TERM", term);
  },
  setWeek({ commit }, week) {
    commit("SET_WEEK", week);
  },
  setAttendance({ commit }, data) {
    commit("SET_ATTENDANCE", data);
  },
  sendAttendance({ state }) {
    axios
      .post("http://localhost:8000/api/attendance", state.class)
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
