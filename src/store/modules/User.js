/* eslint-disable no-console */
import axios from "@/ajax.js";

const state = {
  details: {},
  newUser: {
    role: null
  }
};

const mutations = {
  SET_USER_DETAILS(state, userDetails) {
    state.details = userDetails;
  },
  SET_NEW_ROLE(state, role) {
    state.newUser.role = role;
  }
};

const actions = {
  getUserDetails({ commit, state }, data) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/${data.role}s/${data.id}`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_USER_DETAILS", response.data);
        console.log(response.data);
        console.log(state.details);
      })
      .catch(err => console.log(err));
  },
  sendAdminDetails({ commit }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/admins`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_USER_DETAILS", response.data.data))
      .catch(err => console.log(err));
  },
  sendLecturerDetails({ commit }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/lecturers`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_USER_DETAILS", response.data.data))
      .catch(err => console.log(err));
  },
  sendStudentDetails({ commit }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/students`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_USER_DETAILS", response.data.data))
      .catch(err => console.log(err));
  }
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
