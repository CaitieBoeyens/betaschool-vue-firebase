/* eslint-disable no-console */

import axios from "@/ajax.js";

const state = {
  classes: []
};

const getters = {
  allClasses: state => state.classes
};

const mutations = {
  setLecClasses: (state, classes) => (state.classes = classes)
};

const actions = {
  getClasses({ commit }, user) {
    console.log("getClasses");
    return axios({
      method: "get",
      url: `http://localhost:8000/api/${state.User.role}/${
        state.User.id
      }/classes`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_CLASSES", response.data))
      .catch(err => console.log(err));
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
