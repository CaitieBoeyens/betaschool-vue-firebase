/* eslint-disable no-console */
import axios from "@/ajax.js";

const state = {
  classes: [],
  newClass: {
    day: null,
    name: null,
    school: null,
    startTime: null,
    endTime: null,
    venue: null,
    lecturer: null,
    duration: null,
    price: null
  },
  assigningClass: {
    details: {
      day: null,
      subject: null,
      school: null,
      startTime: null,
      endTime: null,
      venue: null,
      lecturer: null,
      duration: null,
      price: null,
      id: null
    },
    assignedStudents: []
  },
  editingClass: {
    day: null,
    subject: null,
    school: null,
    startTime: null,
    endTime: null,
    venue: null,
    lecturer: null,
    students: [],
    duration: null,
    price: null,
    id: null
  },
  available: {
    students: [],
    lecturers: [],
    venues: [],
    schools: [],
    times: [
      "08:00",
      "09:00",
      "10:00",
      "11:00",
      "12:00",
      "13:00",
      "14:00",
      "15:00",
      "16:00",
      "17:00"
    ],
    days: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
    durations: ["1 hour", "2 hours", "3 hours", "4 hours"]
  }
};

const mutations = {
  SET_CLASSES(state, classes) {
    state.classes = classes;
  },
  SET_ALL_STUDENTS(state, students) {
    state.available.students = students;
  },
  SET_LECTURERS(state, lecturers) {
    state.available.lecturers = lecturers;
  },
  SET_SCHOOLS(state, schools) {
    state.available.schools = schools;
  },

  SET_VENUES(state, venues) {
    state.available.venues = venues;
  },

  SET_ADMINS(state, admins) {
    state.available.admins = admins;
  },

  /* ASSIGNING STUDENTS */
  RESET_ASSIGNING_CLASS(state) {
    state.assigningClass = {
      details: {
        day: null,
        subject: null,
        school: null,
        startTime: null,
        endTime: null,
        venue: null,
        lecturer: null,
        duration: null,
        price: null,
        id: null
      },
      assignedStudents: []
    };
  },
  SET_ASSIGNING_CLASS(state, details) {
    state.assigningClass.details = details;
  },
  ASSIGN_STUDENT(state, student) {
    state.assigningClass.assignedStudents.push(student);
  },
  UNASSIGN_STUDENT(state, student) {
    let idx = state.assigningClass.assignedStudents.indexOf(student);
    state.assigningClass.assignedStudents.splice(idx, 1);
  },
  RESET_ASSIGNED_STUDENTS(state) {
    state.assigningClass.assignedStudents = [];
  },
  SET_ASSIGNED_STUDENTS(state, students) {
    state.assigningClass.assignedStudents = students;
  },

  /* NEW CLASSES */
  RESET_NEW_CLASS(state) {
    state.newClass = {
      day: null,
      name: null,
      school: null,
      startTime: null,
      endTime: null,
      venue: null,
      lecturer: null,
      price: null
    };
  },
  SET_NEW_START_TIME(state, time) {
    state.newClass.startTime = time;
  },
  SET_NEW_DURATION(state, time) {
    state.newClass.duration = time;
  },
  SET_NEW_END_TIME(state) {
    if (state.newClass.duration === "1 hour") {
      state.newClass.endTime = `${parseInt(state.newClass.startTime) + 1}`;
    } else if (state.newClass.duration === "2 hours") {
      state.newClass.endTime = `${parseInt(state.newClass.startTime) + 2}`;
    } else if (state.newClass.duration === "3 hours") {
      state.newClass.endTime = `${parseInt(state.newClass.startTime) + 3}`;
    } else if (state.newClass.duration === "4 hours") {
      state.newClass.endTime = `${parseInt(state.newClass.startTime) + 4}`;
    }
  },
  SET_NEW_DAY(state, day) {
    state.newClass.day = day;
  },
  SET_NEW_SUBJECT(state, subject) {
    state.newClass.name = subject;
  },
  SET_NEW_LECTURER(state, lecturer) {
    state.newClass.lecturer = lecturer;
  },
  SET_NEW_VENUE(state, venue) {
    state.newClass.venue = venue;
  },
  SET_NEW_SCHOOL(state, school) {
    state.newClass.school = school;
  },
  SET_NEW_PRICE(state, price) {
    state.newClass.price = price;
  },

  /* EDITING CLASS DETAILS */
  RESET_EDITING_CLASS(state) {
    state.editingClass = {
      day: null,
      subject: null,
      school: null,
      startTime: null,
      endTime: null,
      venue: null,
      lecturer: null,
      students: [],
      duration: null,
      price: null,
      id: null
    };
  },

  SET_EDITING_START_TIME(state, time) {
    state.editingClass.startTime = time;
  },
  SET_EDITING_DURATION(state, time) {
    state.editingClass.duration = time;
  },
  SET_EDITING_END_TIME(state) {
    if (state.editingClass.duration === 1) {
      state.editingClass.endTime = parseInt(state.editingClass.startTime) + 1;
    } else if (state.editingClass.duration === 2) {
      state.editingClass.endTime = parseInt(state.editingClass.startTime) + 2;
    } else if (state.editingClass.duration === 3) {
      state.editingClass.endTime = parseInt(state.editingClass.startTime) + 3;
    } else if (state.editingClass.duration === 4) {
      state.editingClass.endTime = parseInt(state.editingClass.startTime) + 4;
    }
  },
  SET_EDITING_DAY(state, day) {
    state.editingClass.day = day;
  },
  SET_EDITING_SUBJECT(state, subject) {
    state.editingClass.subject = subject;
  },
  SET_EDITING_LECTURER(state, lecturer) {
    state.editingClass.lecturer = lecturer;
  },
  SET_EDITING_VENUE(state, venue) {
    state.editingClass.venue = venue;
  },
  SET_EDITING_SCHOOL(state, school) {
    state.editingClass.school = school;
  },
  SET_EDITING_PRICE(state, price) {
    state.editingClass.price = price;
  },

  SET_EDITING_CLASS(state, data) {
    state.editingClass = data;
  },

  /* DELETE CLASS */
  DELETE_CLASS(state, details) {
    let idx = state.classes.indexOf(details);
    state.classes.splice(idx, 1);
  }
};

const actions = {
  /* GET ALL */

  getClasses({ commit }, payload) {
    console.log(payload.role);
    return axios({
      method: "get",
      url: `http://localhost:8000/api/${payload.role}/${payload.id}/classes`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_CLASSES", response.data))
      .catch(err => console.log(err));
  },
  getAllClasses({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/classes`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_CLASSES", response.data))
      .catch(err => console.log(err));
  },
  getStudents({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/students`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_ALL_STUDENTS", response.data);
      })
      .catch(err => console.log(err));
  },
  getNewLecturers({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/${state.newClass.school}/lecturers`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_LECTURERS", response.data);
      })
      .catch(err => console.log(err));
  },
  getEditingLecturers({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/${
        state.editingClass.school.id
      }/lecturers`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_LECTURERS", response.data);
      })
      .catch(err => console.log(err));
  },
  getLecturers({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/lecturers`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_LECTURERS", response.data);
      })
      .catch(err => console.log(err));
  },
  getSchools({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/schools`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_SCHOOLS", response.data);
      })
      .catch(err => console.log(err));
  },

  getVenues({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/venues`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_VENUES", response.data);
      })
      .catch(err => console.log(err));
  },

  getAdmins({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/admins`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_ADMINS", response.data);
      })
      .catch(err => console.log(err));
  },

  /* ASSIGNING STUDENTS */

  resetAssignedStudents({ commit }) {
    commit("RESET_ASSIGNED_STUDENTS");
  },

  assignStudent({ commit }, student) {
    commit("ASSIGN_STUDENT", student);
  },
  unassignStudent({ commit }, student) {
    commit("UNASSIGN_STUDENT", student);
  },

  setAssigningClass({ commit }, id) {
    const details = state.classes.find(c => c.id === id);
    commit("SET_ASSIGNING_CLASS", details);

    commit("RESET_ASSIGNED_STUDENTS");
    if (state.assigningClass.details.id) {
      commit("SET_ASSIGNED_STUDENTS", details.students);
    }
  },

  sendAssigningClass({ state }) {
    const data = {
      id: state.assigningClass.details.id,
      student: state.assigningClass.assignedStudents
    };
    axios
      .put(
        `http://localhost:8000/api/studentClasses/${state.assigningClass.details.id}`,
        data
      )
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  },

  /* NEW CLASSES */

  setNewStartTime({ commit }, time) {
    commit("SET_NEW_START_TIME", time);
    commit("SET_NEW_END_TIME");
  },
  setNewDuration({ commit, state }, time) {
    commit("SET_NEW_DURATION", time);
    if (state.newClass.startTime) {
      commit("SET_NEW_END_TIME");
    }
  },
  setNewDay({ commit }, day) {
    commit("SET_NEW_DAY", day);
  },
  setNewSubject({ commit }, subject) {
    commit("SET_NEW_SUBJECT", subject);
  },
  setNewLecturer({ commit }, lecturer) {
    commit("SET_NEW_LECTURER", lecturer);
  },
  setNewVenue({ commit }, venue) {
    commit("SET_NEW_VENUE", venue);
  },
  setNewSchool({ commit }, school) {
    commit("SET_NEW_SCHOOL", school);
  },
  setNewPrice({ commit }, price) {
    commit("SET_NEW_PRICE", price);
  },
  sendNewClass({ state }) {
    const newClass = {
      day: state.newClass.day,
      name: state.newClass.name,
      school_id: state.newClass.school,
      startTime: parseInt(state.newClass.startTime),
      endTime: parseInt(state.newClass.endTime),
      venue_id: state.newClass.venue,
      lecturer_id: state.newClass.lecturer,
      price: state.newClass.price,
    };
    console.log(newClass);
    axios
      .post("http://localhost:8000/api/classes", newClass)
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  },

  /* EDITING CLASSES */

  setEditingStartTime({ commit }, time) {
    commit("SET_EDITING_START_TIME", time);
    commit("SET_EDITING_END_TIME");
  },
  setEditingDuration({ commit, state }, time) {
    commit("SET_EDITING_DURATION", time);
    if (state.newClass.startTime) {
      commit("SET_EDITING_END_TIME");
    }
  },
  setEditingDay({ commit }, day) {
    commit("SET_EDITING_DAY", day);
  },
  setEditingSubject({ commit }, subject) {
    commit("SET_EDITING_SUBJECT", subject);
  },
  setEditingLecturer({ commit }, lecturer) {
    commit("SET_EDITING_LECTURER", lecturer);
  },
  setEditingVenue({ commit }, venue) {
    commit("SET_EDITING_VENUE", venue);
  },
  setEditingSchool({ commit }, school) {
    commit("SET_EDITING_SCHOOL", school);
  },
  setEditingPrice({ commit }, price) {
    commit("SET_EDITING_PRICE", price);
  },

  sendEditingClass({ state }) {
    axios
      .put(
        `http://localhost:8000/api/classes/${state.editingClass.id}`,
        state.editingClass
      )
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  },
  setEditingClass({ commit }, id) {
    const details = state.classes.find(c => c.id === id);
    commit("SET_EDITING_CLASS", details);
  },

  /* DELETE CLASS */

  deleteClass({ commit }, id) {
    const details = state.classes.find(c => c.id === id);
    axios
      .delete(`http://localhost:8000/api/classes/${id}`)
      .then(() => {
        commit("DELETE_CLASS", details);
        console.log("delete");
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
