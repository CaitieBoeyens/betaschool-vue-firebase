/* eslint-disable no-console */
import AuthHelper from "@/auth/helpers";
import axios from "@/ajax.js";

const state = {
  currentUser: null,
  authToken: "",
  rememberUser: false,
  userRole: null
};
const getters = {
  user_is_authenticated: state => {
    return AuthHelper.token_is_valid(state.authToken);
  },
  user_claims: state => {
    if (!state.authToken) return {};
    return AuthHelper.getClaimsFromToken(state.authToken);
  }
};
const mutations = {
  SET_CURRENT_USER_DATA(state, data) {
    state.currentUser = data;
  },
  SET_AUTH_TOKEN(state, token) {
    state.authToken = token;
  },
  SET_REMEMBER_USER(state, new_status) {
    state.rememberUser = new_status;
  },
  SET_USER_ROLE(state, role) {
    state.userRole = role;
  }
};
const actions = {
  login({ commit, getters }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/auth/login`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_AUTH_TOKEN", response.data.token);
        axios.defaults.headers.common["Authorization"] = response.data.token;
        commit("SET_USER_ROLE", response.data.role);
        commit("SET_CURRENT_USER_DATA", getters.user_claims.sub);
      })
      .catch(err => console.log(err));
  },
  register({ dispatch }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/auth/register`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.status);
        if (response.status === 200) {
          const mail = {
            email: data.email,
            password: data.password,
            role: data.role
          };
          dispatch("mail", mail);
        }
      })
      .catch(err => console.log(err));
  },
  // eslint-disable-next-line no-unused-vars
  mail({ _ }, data) {
    return axios({
      method: "post",
      url: `http://localhost:8000/api/auth/mail`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.data);
      })
      .catch(err => console.log(err));
  },
  logout({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:8000/api/auth/refresh`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.status);
        if (response.status === 200) {
          commit("SET_AUTH_TOKEN", "");
          axios.defaults.headers.common["Authorization"] = "";
          commit("SET_USER_ROLE", null);
          commit("SET_CURRENT_USER_DATA", null);
        }
      })
      .catch(err => console.log(err));
  },
  // eslint-disable-next-line no-unused-vars
  updatePassword({ _ }, data) {
    console.log(data);
    return axios({
      method: "put",
      url: `http://localhost:8000/api/users/changePassword`,
      data: data,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        console.log(response.data);
      })
      .catch(err => console.log(err));
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
