/* eslint-disable no-console */
import axios from "@/ajax.js";

const state = {
  class: {
    id: null,
    term: null,
    students: [],
    average: null,
    name: null
  },
  classes: []
};

const mutations = {
  SET_CLASS(state, fullClass) {
    state.class.id = fullClass.id;
    state.class.name = fullClass.name;
  },
  SET_STUDENTS(state, students) {
    students.forEach(student => {
      state.class.students.push({
        ...student,
        mark: 0
      });
    });
  },
  SET_TERM(state, term) {
    state.class.term = term;
  },
  SET_MARK(state, data) {
    const student = state.class.students.find(s => s.user_id === data.id);
    student.mark = data.mark;
  },
  RESET_CLASS(state) {
    state.class = {
      id: null,
      term: null,
      students: [],
      average: null,
      subject: null
    };
  }
};

const actions = {
  /* getStudents({ commit }) {
    return axios({
      method: "get",
      url: `http://localhost:3000/students`,
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        commit("SET_STUDENTS", response.data);
      })
      .catch(err => console.log(err));
  }, */
  setClass({ commit }, fullClass) {
    commit("SET_CLASS", fullClass);
    commit("SET_STUDENTS", fullClass.students);
  },
  setTerm({ commit }, term) {
    commit("SET_TERM", term);
  },
  setMark({ commit }, data) {
    commit("SET_MARK", data);
  },
  sendMarks({ state }) {
    axios
      .post("http://localhost:8000/api/studentMarks", state.class)
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
