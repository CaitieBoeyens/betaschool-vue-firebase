/* eslint-disable no-console */
import axios from "@/ajax.js";

const state = {
  fees: [],
  newQuote: {
    id: "",
    studentNr: null,
    amountDue: null,
    amountPaid: null
  },
  recordTrans: {
    id: "",
    studentNr: null,
    amountPaid: null
  }
};

const mutations = {
  SET_FEES(state, fees) {
    state.fees = fees;
  },
  SET_NEW_AMOUNTDUE(state, amountDue) {
    state.newQuote.amountDue = amountDue;
  },
  SET_NEW_AMOUNTPAID(state, amountPaid) {
    state.newQuote.amountPaid = amountPaid;
  },
  SET_NEW_STUDENTNR(state, studentNr) {
    state.newQuote.studentNr = studentNr;
  }
};

const actions = {
  getFees({ commit }) {
    return axios({
      method: "get",
      url: "http://localhost:3000/fees",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => commit("SET_FEES", response.data))
      .catch(err => console.log(err));
  },
  setNewAmountDue({ commit }, amountDue) {
    commit("SET_NEW_AMOUNTDUE", amountDue);
  },
  setNewAmountPaid({ commit }, amountPaid) {
    commit("SET_NEW_AMOUNTPAID", amountPaid);
  },
  setNewStudentNr({ commit }, studentNr) {
    commit("SET_NEW_STUDENTNR", studentNr);
  },
  sendNewQuote({ state }) {
    axios
      .post("http://localhost:3000/fees", state.newQuote)
      .then(resp => {
        console.log(resp.data);
      })
      .catch(error => {
        console.log(error);
      });
  }
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
