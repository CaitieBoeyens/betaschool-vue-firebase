import Vue from "vue";
import Vuex from "vuex";

import User from "./modules/User";
import Classes from "./modules/Classes";
import Lecturer from "./modules/Lecturer";
import Marks from "./modules/Marks";
import Attendance from "./modules/Attendance";
import Fees from "./modules/Fees";
import Auth from "./modules/Auth";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    User,
    Classes,
    Lecturer,
    Marks,
    Attendance,
    Fees,
    Auth
  }
});
