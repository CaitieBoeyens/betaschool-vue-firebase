import axios from 'axios'
import { AjaxConfig, AuthenticationConfig } from '@/AppConfig'

import {
    token_is_valid,
    auth_token,
    goBackOnePage,
    currently_at_login,
    refreshToken,
    logout
} from './methods'

const AjaxBase = axios.create({
    ...AjaxConfig
})

AjaxBase.interceptors.request.use(
    function(config) {
        const token = auth_token()
        if (token_is_valid(token)) {
            config.headers.common.Authorization = token
        }
        return config
    },
    function(error) {
        return Promise.reject(error)
    }
)

AjaxBase.interceptors.response.use(
    function(response) {
        if (response.headers.authorization) {
            const new_token = response.headers.authorization
            console.log('Token refreshed')
            refreshToken(new_token)
        }
        return response
    },
    function(error) {
        if (error.response) {
            switch (error.response.status) {
                case 401:
                    if (
                        AuthenticationConfig.redirect_when_unauthenticated &&
                        !currently_at_login()
                    ) {
                        logout()
                    } else if (currently_at_login()) {
                        error.validation_errors = error.response.data
                    }
                    break
                case 403:
                    if (!currently_at_login()) {
                        goBackOnePage()
                    }
                    break
                case 422:
                    error.validation_errors = {}
                    for (const field in error.response.data.errors) {
                        const field_name = field.replace('data.', '')
                        error.validation_errors[field_name] =
                            error.response.data.errors[field]
                    }
                    break
                default:
                    break
            }
        }
        return Promise.reject(error)
    }
)
export default AjaxBase
