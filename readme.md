# Betaschool

Betaschool is a school management system developed by Caitie Boeyens (160032) Brian Irons (170201) and Zandri Gillespie (170052).

## Installation

Use the package manager npm to install the dependencies.

```bash
npm install
```

## Usage

Launch the server

```bash
npm run serve
```

After setting up the back end you can log in with any of the following users in order to explore the system:

Admin:  anderson@university.com; Adam Anderson

Student: m.smith@university.com; Monica Smith

Lecturer: bahr.a@university.com; Arthur Bahr

The password for all of the users is 123456
